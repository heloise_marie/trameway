## WebliPack Starter
### Cloner le dépôt

```bash
git clone [URL_DU_REPO]
cd weblipack
```
### Naviguer vers le répertoire du projet
```
cd weblipack
```
### Configuration et Installation
```bash
# Supprimer le répertoire .git 
# Initialiser un nouveau projet Git
git init 

# Installer les dépendances avec NPM
npm install
```
### Exécution de Webpack
```bash
# En mode développement
npm run watch

# En mode production
npm run build
```
### Exécution du serveur PHP
```bash
php -S localhost:2323 -t public
```
