<?php
require('../inc/function.php');

include('../view/header.php'); ?>

    <style>
        h1 {text-align: center;font-size: 3rem;margin: 3rem 0;}
        .logo {margin: 0 auto;max-width: 100px;}
        .logo img{width: 100%;}
    </style>

    <div class="wrap">
        <div class="flexslider">
            <ul class="slides">
                <li>
                    <img src="./asset/images/kitchen_adventurer_donut.jpg" />
                </li>
                <li>
                    <img src="./asset/images/kitchen_adventurer_cheesecake_brownie.jpg" />
                </li>
                <li>
                    <img src="./asset/images/kitchen_adventurer_caramel.jpg" />
                </li>
            </ul>
        </div>
    </div>

    <h1>WebliPack Starter</h1>
    <div class="logo">
        <img src="./asset/img/monster-weblipack.png" alt="logo weblitpack">
    </div>

<?php include('../view/footer.php');
